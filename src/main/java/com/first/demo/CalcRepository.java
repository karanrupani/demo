package com.first.demo;

import org.springframework.data.repository.CrudRepository;

public interface CalcRepository extends CrudRepository<Calc, Long> {
	Calc findById(long id);
}