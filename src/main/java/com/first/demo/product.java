package com.first.demo;

public class product {
	private int id;
	private String pname;
	private String batchno;
	private double price;
	private int noofproduct;  

	
	public product() {
		
	}
	
	public product(int id, String pname, String batchno, double price, int noofproduct) {
		super();
		this.setId(id);  
		this.setPname(pname);  
		this.setBatchno(batchno);  
		this.setPrice(price);  
		this.setNoofproduct(noofproduct);		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getNoofproduct() {
		return noofproduct;
	}

	public void setNoofproduct(int noofproduct) {
		this.noofproduct = noofproduct;
	}

	public String getBatchno() {
		return batchno;
	}

	public void setBatchno(String batchno) {
		this.batchno = batchno;
	}
	
	
}
