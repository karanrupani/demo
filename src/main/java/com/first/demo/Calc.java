package com.first.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;



@Entity
public class Calc {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@NotNull
	@Min(1000)
	private Integer loanAmount;

	
	
	@NotNull
	@Min(1)
	@Max(30)
	private Integer tenure;
	
	@NotNull
	@Min(1)
	@Max(20)
	private Double interestRate;
	
	protected Calc() {}

	public Calc(Integer loanAmount, Integer tenure, Double interestRate) {
//		super();
		this.setLoanAmount(loanAmount);
		this.setTenure(tenure);
		this.setInterestRate(interestRate);
	}
	
	public Long getId() {
		return id;
	}
	
	public Integer getTenure() {
		return tenure;
	}

	public void setTenure(Integer tenure) {
		this.tenure = tenure;
	}

	public Integer getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Integer loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}
}
