package com.first.demo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class CalculatorController {
	
	@Autowired
	CalcRepository calcRepo;
	@Autowired
    private PaymentService paymentService;
    
	@RequestMapping(value="/calc", method = RequestMethod.GET)	
    public String calc(Calc calc) {
		return "calc";
	}
    
    @RequestMapping(value="/calc", method = RequestMethod.POST)
	public String saveCalc(@Valid Calc calc, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
    	if (bindingResult.hasErrors()) {
			return "calc";
		}

    	calc = calcRepo.save(calc);
    	System.out.println(calc.getId());
    	redirectAttributes.addAttribute("id", calc.getId());
    	return "redirect:/showEmiSchedule";
	}
    
    @RequestMapping(value="/showEmiSchedule", method = RequestMethod.GET)
	public String showEmiSchedule(@RequestParam("id") long id, Model model) {
    	Calc calc = calcRepo.findById(id);
        int initialBalance, time, durationInMonths;
        double interestRate;
        initialBalance = calc.getLoanAmount();
        time = calc.getTenure();
        interestRate = calc.getInterestRate();
        durationInMonths = time*12; 
//        monthlyEMI = (principal*rate*Math.pow(1+rate,time))/(Math.pow(1+rate,time)-1);

        // compute monthly payment
        double monthlyPayment = paymentService.pmt(paymentService.getMonthlyInterestRate(interestRate), durationInMonths, initialBalance, 0, 0);
        model.addAttribute("monthlyEmi", Math.abs(monthlyPayment));        
        
        // calculate detailed payment list
        Date date = new Date();
        List<Payment> paymentList = calculatePaymentList(date, initialBalance, durationInMonths, 0, interestRate, 0);
        model.addAttribute("paymentList", paymentList);        
    	return "schedule";
	}
    
    public List<Payment> calculatePaymentList(Date startDate, double initialBalance, int durationInMonths, int paymentType, double interestRate, double futureValue)
    {
        List<Payment> paymentList = new ArrayList<Payment>();
        Date loopDate = startDate;
        double balance = initialBalance;
        double accumulatedInterest = 0;
        for (int paymentNumber = 1; paymentNumber <= durationInMonths; paymentNumber++)
        {
            if (paymentType == 0)
            {
                loopDate = addOneMonth(loopDate);
            }
            double principalPaid = paymentService.ppmt(paymentService.getMonthlyInterestRate(interestRate), paymentNumber, durationInMonths, initialBalance, futureValue, paymentType);
            double interestPaid = paymentService.ipmt(paymentService.getMonthlyInterestRate(interestRate), paymentNumber, durationInMonths, initialBalance, futureValue, paymentType);
            balance = balance + principalPaid;
            accumulatedInterest += interestPaid;

            Payment payment = new Payment(paymentNumber, loopDate, balance, principalPaid, interestPaid, accumulatedInterest);

            paymentList.add(payment);

            if (paymentType == 1)
            {
                loopDate = addOneMonth(loopDate);
            }
        }
        return paymentList;
    }
    
    /**
     * Adds a month to a date, for purposes of putting a date on each Payment object.
     * @param date any arbitrary date
     * @return Date a date one month later
     */
    private Date addOneMonth(Date date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        return cal.getTime();
    }
}
